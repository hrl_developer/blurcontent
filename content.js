var blur = 7;
var keys = []

/** Обработчике на нажатие левой кнопки мыши **/
window.onmousedown = function(event) {
  var element = event.target || event.srcElement; // Элемент на который наведён курсор мыши

  if (keys[17]) {
    if (element.style.filter == `blur(${blur}px)`) {
      element.style.filter = `blur(0px)`;
    } else {
      element.style.filter = `blur(${blur}px)`;
    }
  }
}

/** Обработчики на клавитуру, нажатие, отжатие клавиш **/
window.onkeydown = function(event) {
  keys[event.keyCode] = true;
}

window.onkeyup = function(event) {
  keys[event.keyCode] = false;
}
